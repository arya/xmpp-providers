<!--
SPDX-FileCopyrightText: 2023 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Tools

[[_TOC_]]

## Filter Script

The script `filter.py` can be used to create filtered lists of providers (called **provider lists**).

### Usage

You can see all options for running the script:
```
./filter.py -h
```

Provider lists for all categories are created if you run the script without arguments:
```
./filter.py
```

If you want to create a filtered list which can be used by a client, simply enter the category name.

Example for creating a list of category **A** providers:
```
./filter.py -A
```

You can create a provider list containing all providers for completely customized filtering (e.g., by an own filter script or at runtime):
```
./filter.py -C
```

A **s**imple list containing only the domains of the providers is also possible.

Example for creating a domain list of category **D** providers to use it only for autocomplete:
```
./filter.py -s -C
```

The **c**ategories of the filtered providers can be included in their entries.

Example for creating a list of category **D** providers that includes their best categories.
```
./filter.py -c -D
```

You can create files containing the **r**esults of the filtering (called **categorization results**).
Those files include the providers' properties that do not meet the criteria for being in specific categories.

Example for creating result files for all providers:
```
./filter.py -r
```

If you are interested in specific providers, you can append them to the command.

Example for creating a list of category **B** providers out of *example.org* and *example.com*:
```
./filter.py -B example.org example.com
```

The script can be run in **d**ebug mode to see why providers are not in a specific category.

Example for creating a list of category **A** providers and logging additional information:
```
./filter.py -d -A
```

Furthermore, the arguments can be combined to show which criteria specific providers do not meet for being in a specific category.

Example for creating a list of category **A** providers out of *example.org* and *example.com* and logging additional information:
```
./filter.py -d -A example.org example.com
```

## Badge Script

The script `badge.py` can be used to create badges for the specified categories.
It uses the files created by the filter script.
Thus, the filter script must be run before running the badge script.

Template files are used for generating the badges.
All badges contain the category.
Additionally, it is possible to create badges containing the count of providers in a specific category.

The badge script has two phases:
1. It generates badges for all categories.
1. It creates the directory `badges` and fills it with badges for all providers (called **provider badges**).

### Usage

You can see all options for running the script:
```
./badge.py -h
```

Badges for all categories are created if you run the script without arguments:
```
./badge.py
```

The badges for providers can be created as symbolic **l**inks instead of regular files:
```
./badge.py -l
```

If you want to create a badge which can be used by a provider, simply enter the category name.

Example for creating a badge for category **A**:
```
./badge.py -A
```

Badges containing the **c**ount of providers in specific categories are also possible.

Example for creating a badge containing the count of providers in category **C** in addition to the normal badges:
```
./badge.py -c -C
```

The script can be run in **d**ebug mode to see detailed information about the process.

Example for creating a badge for category **A** and logging additional information:
```
./badge.py -d -A
```

## URL Checker

The script `check_urls.py` can be used to check URLs.
It succeeds if all checked URLs are reachable.

### Usage

You can see all options for running the script:
```
./check_urls.py -h
```

The URLs of all appropriate files are checked if you run the script without arguments:
```
./check_urls.py
```

It is also possible to check only the URLs in specific files.

Example for checking the URLs in `providers.json` and `filter.py`:
```
./check_urls.py providers.json filter.py
```

## Automation

The providers file is automatically kept up up-do-date via **automation tools**.
They retrieve data from external sources and write changes to the providers file.

### Web

The script `automation/web.py` can be used to query provider ratings of external services.

#### Usage

The ratings are queried and the corresponding properties in the providers file changed (when necessary) if you run the script without arguments:
```
python3 -m automation.web
```

You can also trigger **u**pdates of ratings supporting that (querying and updating a rating cannot be done at the same time):
```
python3 -m automation.web -u
```
