#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This script generates badges for specific categories.
"""
from __future__ import annotations

from typing import Any
from typing import cast

import json
import logging
import os
import shutil
import sys
from argparse import ArgumentParser
from pathlib import Path

from common import Category
from common import get_logger
from common import PROVIDERS_FILE_PATH

BADGE_TEMPLATE_FILE_PATH = "badge.svg"
BADGE_COUNT_TEMPLATE_FILE_PATH = "badge-count.svg"

PROVIDER_BADGES_DIRECTORY_PATH = "badges"

CATEGORY_PLACEHOLDER = "category-placeholder"
COLOR_PLACEHOLDER = "color-placeholder"
COUNT_PLACEHOLDER = "count-placeholder"

CATEGORIES_AND_COLORS = {
    Category.AUTOMATICALLY_CHOSEN: "439639",
    Category.MANUALLY_SELECTABLE: "a0ce67",
    Category.COMPLETELY_CUSTOMIZABLE: "e96d1f",
    Category.AUTOCOMPLETE: "d9101e",
}


class BadgeGeneratorArgumentParser(ArgumentParser):
    """Parser for arguments provided to run the application."""

    def __init__(self) -> None:
        super().__init__()

        self.description = """Generates badges for specific categories based on
            the provider lists which must be created beforehand."""
        self.usage = "%(prog)s [-h] | [-q | -d] [-l] [-c] [-A | -B | -C | -D]"

        self.add_argument(
            "-q",
            "--quiet",
            help="log only errors",
            action="store_const",
            dest="log_level",
            const=logging.ERROR,
            default=logging.INFO,
        )

        self.add_argument(
            "-d",
            "--debug",
            help="log debug output",
            action="store_const",
            dest="log_level",
            const=logging.DEBUG,
            default=logging.INFO,
        )

        self.add_argument(
            "-l",
            "--links",
            help="create symbolic links to badges instead of creating regular files",
            action="store_true",
            dest="create_symlink",
        )

        self.add_argument(
            "-c",
            "--count",
            help="""generate badges with count of providers in specific categories
                in addition to the normal badges""",
            action="store_true",
            dest="add_count",
        )

        self.add_argument(
            "-A",
            "--category-A",
            help="generate only badge for category A",
            action="store_const",
            dest="category",
            const=Category.AUTOMATICALLY_CHOSEN,
            default=Category.ALL,
        )

        self.add_argument(
            "-B",
            "--category-B",
            help="generate only badge for category B",
            action="store_const",
            dest="category",
            const=Category.MANUALLY_SELECTABLE,
            default=Category.ALL,
        )

        self.add_argument(
            "-C",
            "--category-C",
            help="generate only badge for category C",
            action="store_const",
            dest="category",
            const=Category.COMPLETELY_CUSTOMIZABLE,
            default=Category.ALL,
        )

        self.add_argument(
            "-D",
            "--category-D",
            help="generate only badge for category D",
            action="store_const",
            dest="category",
            const=Category.AUTOCOMPLETE,
            default=Category.ALL,
        )


class BadgeGenerator:
    """Creates badges using template files.

    Parameters
    ----------
    input_category : Category
        category to generate badges for
    add_count : bool
        whether to generate category count badges
    create_symlink : bool
        whether to create symbolic links for badges instead of regular files
    """

    def __init__(
        self,
        input_category: Category,
        add_count: bool,
        create_symlink: bool,
    ) -> None:

        self._initialize_directory(Path(PROVIDER_BADGES_DIRECTORY_PATH))

        categories = [input_category]

        if input_category == Category.ALL:
            categories = [
                Category.AUTOMATICALLY_CHOSEN,
                Category.MANUALLY_SELECTABLE,
                Category.COMPLETELY_CUSTOMIZABLE,
                Category.AUTOCOMPLETE,
            ]

        for category in categories:
            providers = self._get_providers_for_category(category)
            self._create_provider_badges(category, providers, create_symlink)

            if add_count:
                self._create_count_badge(category, len(providers))

    @staticmethod
    def _initialize_directory(path: Path) -> None:
        """Initializes a directory by emptying it.

        Parameters
        ----------
        path : Path
            path of the directory to initialize
        """

        if path.exists() and path.is_dir():
            log.debug("Removing directory '%s'", path)
            shutil.rmtree(path)

        log.debug("Recreating directory '%s'", path)
        os.mkdir(path)

    def _create_count_badge(self, category: Category, count: int) -> None:
        """Creates a badge for a category with its provider count.

        Parameters
        ----------
        category : Category
            provider's category
        count : int
            number of providers in this category
        """

        badge_count_template_file_path = Path(BADGE_COUNT_TEMPLATE_FILE_PATH)
        stem = badge_count_template_file_path.stem
        suffix = badge_count_template_file_path.suffix
        badge_file_path = f"{stem}-{category.value}{suffix}"

        with open(badge_file_path, "w") as badge_file:
            log.debug(
                "Creating badge for category %s with color %s and count %s",
                category.value,
                CATEGORIES_AND_COLORS[category],
                str(count),
            )

            badge_content = self._get_template_content(badge_count_template_file_path)
            badge_content = badge_content.replace(CATEGORY_PLACEHOLDER, category.value)
            badge_content = badge_content.replace(
                COLOR_PLACEHOLDER, CATEGORIES_AND_COLORS[category]
            )
            badge_content = badge_content.replace(COUNT_PLACEHOLDER, str(count))

            badge_file.write(badge_content)
            log.info("'%s' created", badge_file_path)

    def _create_provider_badges(
        self, category: Category, providers: list[str], create_symlink: bool
    ) -> None:
        """Creates a badge for each provider in a category.

        Parameters
        ----------
        category : Category
            provider's category
        providers : list[str]
            list of providers in this category
        create_symlink : bool
            whether to create symbolic links for badges or regular files
        """

        badge_template_file_path = Path(BADGE_TEMPLATE_FILE_PATH)
        stem = badge_template_file_path.stem
        suffix = badge_template_file_path.suffix
        badge_file_path = f"{stem}-{category.value}{suffix}"

        with open(badge_file_path, "w") as badge_file:
            badge_content = self._get_template_content(badge_template_file_path)
            badge_content = badge_content.replace(CATEGORY_PLACEHOLDER, category.value)
            badge_content = badge_content.replace(
                COLOR_PLACEHOLDER, CATEGORIES_AND_COLORS[category]
            )

            badge_file.write(badge_content)
            log.info("'%s' created", badge_file_path)

        for provider in providers:
            provider_badge_file_path = (
                Path(PROVIDER_BADGES_DIRECTORY_PATH) / f"{provider}{suffix}"
            )

            if provider_badge_file_path.exists():
                log.debug(
                    "Skipping creation of provider badge for category %s "
                    "because it already exists for better category",
                    category.value,
                )
                continue

            self._relocate_badge(
                category, badge_file_path, provider_badge_file_path, create_symlink
            )

    @staticmethod
    def _relocate_badge(
        category: Category, source_path: Path, target_path: Path, create_symlink: bool
    ) -> None:
        """Moves (or symlinks) a generated badge.

        Parameters
        ----------
        category : Category
            provider's category
        source_path : Path
            path of the generated category badge file
        target_path : Path
            path of the provider-named badge file
        create_symlink : bool
            whether to create a symbolic link for target_path
        """

        if create_symlink:
            relative_path = os.path.relpath(source_path, PROVIDER_BADGES_DIRECTORY_PATH)
            log.debug(
                "Creating provider badge link '%s' to '%s' for category %s",
                target_path,
                relative_path,
                category.value,
            )
            os.symlink(relative_path, target_path)

        else:
            log.debug(
                "Creating provider badge file '%s' by copying '%s' for category %s",
                target_path,
                source_path,
                category.value,
            )
            shutil.copyfile(source_path, target_path)

        log.info("'%s' created", target_path)

    @staticmethod
    def _get_template_content(path: Path) -> str:
        """Reads the content of a template file.

        Parameters
        ----------
        path : Path
            template file path

        Returns
        -------
        str
            content of the template file
        """

        log.debug("Using template file '%s'", path)

        with open(path, "r") as file:
            return file.read()

    @staticmethod
    def _get_providers_for_category(category: Category) -> list[str]:
        """Reads the provider list file for a specific category and extracts its
        provider JIDs.

        Parameters
        ----------
        category : Category
            provider's category

        Returns
        -------
        list[str]
            provider JIDs
        """

        providers_file_path = Path(PROVIDERS_FILE_PATH)
        stem = providers_file_path.stem
        suffix = providers_file_path.suffix
        provider_list_file_path = f"{stem}-{category.value}{suffix}"

        with open(provider_list_file_path, "r") as provider_list_file:
            try:
                providers_dict = cast(dict[str, Any], json.load(provider_list_file))
            except json.decoder.JSONDecodeError as error:
                log.error(
                    "'%s' has invalid JSON syntax: %s in line %s at column %s",
                    provider_list_file_path,
                    error.msg,
                    error.lineno,
                    error.colno,
                )
                sys.exit(1)

        providers_list: list[str] = []
        for provider in providers_dict:
            providers_list.append(provider["jid"])

        log.debug(
            "Provider list for category %s contains %s providers",
            category.value,
            len(providers_list),
        )

        return providers_list


if __name__ == "__main__":
    arguments = BadgeGeneratorArgumentParser().parse_args()
    log = get_logger(arguments.log_level)

    BadgeGenerator(
        arguments.category,
        arguments.add_count,
        arguments.create_symlink,
    )
