#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2021 Michel Le Bihan <michel@lebihan.pl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This script filters the providers and extracts those matching specific criteria.
"""
from __future__ import annotations

from typing import Any
from typing import cast

import json
import logging
import os
from argparse import ArgumentParser
from collections import defaultdict
from collections import OrderedDict

from common import Category
from common import create_parent_directories
from common import get_logger
from common import JSON_INDENTATION
from common import PROVIDERS_FILE_PATH
from criteria import CRITERIA_A
from criteria import CRITERIA_B
from criteria import CRITERIA_C
from criteria import CRITERIA_D
from data_types import FilteredDetailsT
from data_types import ProviderDetailsT

DOMAINS_ONLY_FILE_NAME_SUFFIX = "s"
RESULT_FILE_PATH = "results/%s.json"
PROPERTY_CONTENT_KEY = "content"


class ApplicationArgumentParser(ArgumentParser):
    """This is a parser for the arguments provided to run the application."""

    def __init__(self) -> None:
        super().__init__()

        self.description = (
            "Filters the providers and extracts those matching specific criteria."
        )
        self.usage = (
            "%(prog)s [-h] | [-q | -d] [-s | -c] [-r] [-A | -B | -C | -D] [providers]"
        )

        self.add_argument(
            "-q",
            "--quiet",
            help="log only errors",
            action="store_const",
            dest="log_level",
            const=logging.ERROR,
            default=logging.INFO,
        )

        self.add_argument(
            "-d",
            "--debug",
            help="log debug output",
            action="store_const",
            dest="log_level",
            const=logging.DEBUG,
            default=logging.INFO,
        )

        self.add_argument(
            "-s",
            "--simple",
            help="output only provider domains instead of their properties",
            action="store_true",
            dest="simple",
        )

        self.add_argument(
            "-c",
            "--categories",
            help="add the category of each provider to its entry",
            action="store_true",
            dest="categories",
        )

        self.add_argument(
            "-r",
            "--result-files",
            help="create additional files each containing the result of one provider",
            action="store_true",
            dest="result_files",
        )

        self.add_argument(
            "-A",
            "--category-A",
            help="output only providers of category A",
            action="store_const",
            dest="category",
            const=Category.AUTOMATICALLY_CHOSEN,
            default=Category.ALL,
        )

        self.add_argument(
            "-B",
            "--category-B",
            help="output only providers of category B",
            action="store_const",
            dest="category",
            const=Category.MANUALLY_SELECTABLE,
            default=Category.ALL,
        )

        self.add_argument(
            "-C",
            "--category-C",
            help="output only providers of category C",
            action="store_const",
            dest="category",
            const=Category.COMPLETELY_CUSTOMIZABLE,
            default=Category.ALL,
        )

        self.add_argument(
            "-D",
            "--category-D",
            help="output only providers of category D",
            action="store_const",
            dest="category",
            const=Category.AUTOCOMPLETE,
            default=Category.ALL,
        )

        self.add_argument(
            "providers",
            help="domains of providers being filtered",
            nargs="*",
        )


def create_provider_list(
    provider_data: dict[str, ProviderDetailsT],
    providers: list[str],
    category: Category,
    domains_only: bool,
    categories_included: bool,
    results: dict[str, Any],
) -> None:
    """Creates a file for providers of a specific category.

    Parameters
    ----------
    provider_data : dict
        provider data being filtered
    providers : list
        domains of providers being filtered or an empty list for filtering all providers
    category : Category
        category used for filtering
    domains_only : bool
        whether to output a list of provider domains instead of the properties
    categories_included : bool
        whether to add the category of each provider to its entry
    results : dict
        results of the filtering
    """

    log.debug("STARTING creation of provider list for category %s", category.value)

    providers_count = len(providers)
    extracted_providers = []

    for jid, properties in provider_data.items():
        if providers_count == 0 or jid in providers:
            log.debug("  %s: Filtering", jid)

            output_properties = create_output_properties(properties.copy())
            provider_in_category = False

            if categories_included:
                included_categories = [
                    Category.AUTOMATICALLY_CHOSEN,
                ]

                if category != Category.AUTOMATICALLY_CHOSEN:
                    included_categories.append(Category.MANUALLY_SELECTABLE)

                    if category != Category.MANUALLY_SELECTABLE:
                        included_categories.append(Category.COMPLETELY_CUSTOMIZABLE)

                        if category != Category.COMPLETELY_CUSTOMIZABLE:
                            included_categories.append(Category.AUTOCOMPLETE)

                for included_category in included_categories:
                    if filter_provider(
                        included_category,
                        output_properties,
                        results[jid][included_category.value],
                    ):
                        output_properties["category"] = included_category.value
                        output_properties.move_to_end("category", False)
                        provider_in_category = True
                        break
            else:
                if filter_provider(
                    category, output_properties, results[jid][category.value]
                ):
                    provider_in_category = True

            if provider_in_category:
                output_properties["jid"] = jid
                output_properties.move_to_end("jid", False)

                extracted_providers.append(output_properties)

    if providers_count == 0:
        providers_count = len(provider_data)

    log.debug(
        "RESULT: %s of %s providers in category %s",
        len(extracted_providers),
        providers_count,
        category.value,
    )
    log.debug(
        "The criteria are specified in the README:"
        "https://invent.kde.org/melvo/xmpp-providers#criteria"
    )

    if domains_only:
        extracted_providers = [provider["jid"] for provider in extracted_providers]

    # A newline is appended because Python's JSON module does not add one.
    formatted_json_string = (
        json.dumps(extracted_providers, indent=JSON_INDENTATION) + "\n"
    )

    providers_file_path_parts = os.path.splitext(PROVIDERS_FILE_PATH)
    providers_file_name = providers_file_path_parts[0]
    providers_file_extension = providers_file_path_parts[1]

    provider_list_file_path = "%s-%s%s%s" % (
        providers_file_name,
        category.value,
        DOMAINS_ONLY_FILE_NAME_SUFFIX if domains_only else "",
        providers_file_extension,
    )
    with open(provider_list_file_path, "w") as provider_list_file:
        provider_list_file.write(formatted_json_string)
        log.info("'%s' created", provider_list_file_path)


def create_output_properties(properties: ProviderDetailsT) -> FilteredDetailsT:
    """Creates the output properties of a provider consisting only of the information
    relevant to the users.

    Parameters
    ----------
    properties : dict
        properties of the provider

    Returns
    -------
    dict
        only consisting of relevant properties
    """
    new_properties = cast(FilteredDetailsT, OrderedDict())

    for property_name, property_data in properties.items():
        if PROPERTY_CONTENT_KEY in property_data:
            new_properties[property_name] = property_data[PROPERTY_CONTENT_KEY]
        else:
            new_properties[property_name] = {}
            for language_code, language_specific_data in property_data.items():
                content = language_specific_data[PROPERTY_CONTENT_KEY]
                if len(content) != 0:
                    new_properties[property_name][language_code] = content

    return new_properties


def filter_provider(
    category: Category, properties: FilteredDetailsT, results: dict[str, Any]
) -> bool:
    """Filters properties by a passed category.

    Parameters
    ----------
    category : Category
        category used for filtering
    properties : dict
        properties of the provider
    results : dict
        results of the filtering

    Returns
    -------
    bool
        whether the provider belongs to the category
    """

    if category == Category.AUTOCOMPLETE:
        return check_properties(category, properties, CRITERIA_D, results)

    if category == Category.COMPLETELY_CUSTOMIZABLE:
        return check_properties(category, properties, CRITERIA_C, results)

    if category == Category.MANUALLY_SELECTABLE:
        return check_properties(category, properties, CRITERIA_B, results)

    if category == Category.AUTOMATICALLY_CHOSEN:
        return check_properties(category, properties, CRITERIA_A, results)

    return False


def check_properties(
    category: Category,
    properties: FilteredDetailsT,
    criteria: dict[str, Any],
    results: dict[str, Any],
) -> bool:
    """Checks if properties meet specific criteria.

    Parameters
    ----------
    category : Category
        category used for filtering
    properties : dict
        properties of the provider
    criteria : dict
        criteria that are checked
    results : dict
        results of the check

    Returns
    -------
    bool
        whether all properties meet the criteria
    """

    check_succeeded = True

    for property_string, criterion in criteria.items():
        property_names = property_string.split(",")
        property_values = []

        for property_name in property_names:
            property_values.append(properties[property_name])

        if not criterion(*property_values):
            for property_name in property_names:
                property_value = properties[property_name]
                results[property_name] = property_value
                log.debug(
                    "    %s: %s not meeting the criterion for category %s",
                    property_name,
                    property_value,
                    category.value,
                )

            if check_succeeded:
                check_succeeded = False

    return check_succeeded


def create_result_files(results: dict[str, Any]) -> None:
    """Creates files for the results of the filtering.

    Parameters
    ----------
    results: dict
        results of the filtering
    """

    create_parent_directories(RESULT_FILE_PATH)

    for jid, result in results.items():
        # A newline is appended because Python's JSON module does not add one.
        formatted_json_string = json.dumps(result, indent=JSON_INDENTATION) + "\n"

        result_file_path = RESULT_FILE_PATH % jid
        with open(result_file_path, "w") as result_file:
            result_file.write(formatted_json_string)
            log.info("'%s' created", result_file_path)


if __name__ == "__main__":
    arguments = ApplicationArgumentParser().parse_args()
    log = get_logger(arguments.log_level)

    with open(PROVIDERS_FILE_PATH, "r") as providers_file:
        try:
            provider_data = cast(dict[str, ProviderDetailsT], json.load(providers_file))

            category = cast(Category, arguments.category)
            categories = [category]

            if category == Category.ALL:
                categories = [
                    Category.AUTOMATICALLY_CHOSEN,
                    Category.MANUALLY_SELECTABLE,
                    Category.COMPLETELY_CUSTOMIZABLE,
                    Category.AUTOCOMPLETE,
                ]

            results = defaultdict(lambda: defaultdict(lambda: defaultdict(str)))

            for category in categories:
                create_provider_list(
                    provider_data,
                    arguments.providers,
                    category,
                    arguments.simple,
                    arguments.categories,
                    results,
                )

            if arguments.result_files:
                create_result_files(results)

        except json.decoder.JSONDecodeError as e:
            log.error(
                "'%s' has invalid JSON syntax: %s in line %s at column %s",
                PROVIDERS_FILE_PATH,
                e.msg,
                e.lineno,
                e.colno,
            )
