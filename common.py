#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This file includes parts needed by all scripts.
"""
import logging
import os
from enum import Enum

README_FILE_PATH = "README.md"
CONTRIBUTING_FILE_PATH = "CONTRIBUTING.md"
PROVIDERS_FILE_PATH = "providers.json"
CLIENTS_FILE_PATH = "clients.json"

JSON_INDENTATION = 4
LOG_FORMAT = "%(levelname)-8s %(message)s"


class Category(Enum):
    """This contains the provider categories."""

    ALL = "a"
    AUTOMATICALLY_CHOSEN = "A"
    MANUALLY_SELECTABLE = "B"
    COMPLETELY_CUSTOMIZABLE = "C"
    AUTOCOMPLETE = "D"


def get_logger(log_level: int) -> logging.RootLogger:
    """Initializes a logger.

    Parameters
    ----------
    log_level : int
        log level to be used by the logger

    Returns
    -------
    logging.RootLogger
        the initialized logger
    """

    logging.basicConfig(level=log_level, format=LOG_FORMAT)
    return logging.getLogger()


def create_parent_directories(file_path: str) -> None:
    """Creates all parent directories of a file.

    Parameters
    ----------
    file_path : str
        path of the file
    """

    directory_path = os.path.dirname(file_path)

    if len(directory_path) != 0:
        os.makedirs(directory_path, exist_ok=True)
