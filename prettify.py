#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
# SPDX-FileCopyrightText: 2021 Michel Le Bihan <michel@lebihan.pl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
Validates JSON files and applies a consistent format to them.

It is intended as a Git pre-commit hook.
"""
import json
import logging
import sys
from argparse import ArgumentParser

from common import CLIENTS_FILE_PATH
from common import get_logger
from common import JSON_INDENTATION
from common import PROVIDERS_FILE_PATH

PROPERTIES = [
    "lastCheck",
    "website",
    "busFactor",
    "company",
    "passwordReset",
    "inBandRegistration",
    "registrationWebPage",
    "ratingXmppComplianceTester",
    "ratingImObservatoryClientToServer",
    "ratingImObservatoryServerToServer",
    "maximumHttpFileUploadFileSize",
    "maximumHttpFileUploadTotalSize",
    "maximumHttpFileUploadStorageTime",
    "maximumMessageArchiveManagementStorageTime",
    "professionalHosting",
    "freeOfCharge",
    "legalNotice",
    "serverLocations",
    "groupChatSupport",
    "chatSupport",
    "emailSupport",
    "since",
]


class PrettifyArgumentParser(ArgumentParser):
    """Parses arguments for Prettify."""

    def __init__(self) -> None:
        super().__init__()

        self.description = """Validates JSON files and applies a consistent
            format to them.
            """
        self.usage = "%(prog)s [-h] | [-q | -d]"

        self.add_argument(
            "-q",
            "--quiet",
            help="log only errors",
            action="store_const",
            dest="log_level",
            const=logging.ERROR,
            default=logging.INFO,
        )

        self.add_argument(
            "-d",
            "--debug",
            help="log debug output",
            action="store_const",
            dest="log_level",
            const=logging.DEBUG,
            default=logging.INFO,
        )


class Prettify:
    """Validates JSON files and applies a consistent format to them."""

    def __init__(self) -> None:
        all_files_already_correct = True

        for json_file_path in PROVIDERS_FILE_PATH, CLIENTS_FILE_PATH:
            if not self._prettify(json_file_path):
                all_files_already_correct = False

        if not all_files_already_correct:
            sys.exit(1)

    @staticmethod
    def _prettify(json_file_path: str) -> bool:
        """Validates the JSON file from json_file_path and applies a consistent
        format to it.

        Parameters
        ----------
        json_file_path : str

        Returns
        -------
        bool
          whether the JSON file has already been correctly formatted before
        """

        with open(json_file_path, "r+") as json_file:
            original_json_string = json_file.read()

            try:
                # Entries (providers or clients) are sorted in alphabetically ascending
                # and lowercase first order by their keys (domains or names).
                # Properties are sorted in the order specified by PROPERTIES.
                # A newline is appended because Python's JSON module does not add one.
                unsorted_entries = json.loads(original_json_string).items()

            except json.decoder.JSONDecodeError as e:
                log.error(
                    "'%s' has invalid JSON syntax: %s in line %s at column %s: "
                    "Correct syntax, stage changed lines by 'git add -p %s' "
                    "and run 'git commit' again",
                    json_file_path,
                    e.msg,
                    e.lineno,
                    e.colno,
                    json_file_path,
                )

                return False

            sorted_entries = {
                entry_key: {
                    property_key: properties[property_key]
                    for property_key in PROPERTIES
                    if property_key in properties
                }
                for entry_key, properties in sorted(
                    unsorted_entries, key=lambda item: str.swapcase(item[0])
                )
            }
            formatted_json_string = (
                json.dumps(sorted_entries, indent=JSON_INDENTATION) + "\n"
            )

            properties_missing = False

            # Check if provider entries have all needed properties.
            if json_file_path == PROVIDERS_FILE_PATH:
                for provider, properties in sorted_entries.items():
                    for property_key in PROPERTIES:
                        if property_key not in properties.keys():
                            log.error(
                                "%s: Property '%s' is missing",
                                provider,
                                property_key,
                            )
                            if not properties_missing:
                                properties_missing = True

            if original_json_string == formatted_json_string and not properties_missing:
                log.debug("'%s' is already correctly formatted", json_file_path)
            else:
                json_file.seek(0)
                json_file.write(formatted_json_string)
                json_file.truncate()

                if properties_missing:
                    log.error(
                        "'%s' has missing provider properties: "
                        "Add them before the next commit",
                        json_file_path,
                    )

                if original_json_string != formatted_json_string:
                    log.info(
                        "'%s' has been formatted: Stage changed lines by "
                        "'git add -p %s' and run 'git commit'",
                        json_file_path,
                        json_file_path,
                    )

                return False

        return True


if __name__ == "__main__":
    arguments = PrettifyArgumentParser().parse_args()
    log = get_logger(arguments.log_level)

    Prettify()
