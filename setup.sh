#!/bin/sh

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: CC0-1.0

# This script sets up everything needed to work inside of the local repository.

# Set up Git pre-commit hook.

cp pre-commit .git/hooks/

# Install dependencies.

read -p "Do you want to install dependencies via Python package manager pip? [Y/n]" selection

if [ ${#selection} -ne 0 ] && ([ ${selection} = "n" ] || [ ${selection} = "N" ])
then
    exit
fi

pip install -r requirements.txt

if [ $? -ne 0 ]
then
    echo "Dependencies could not be installed. You maybe need to install pip first."
fi
