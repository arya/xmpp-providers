#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Daniel Brötzmann <daniel.broetzmann@posteo.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains an HTTP/REST bot for provider ratings."""

from __future__ import annotations

import json
import logging
import random
import time
from argparse import ArgumentParser
from http import HTTPStatus

import requests
from bs4 import BeautifulSoup

from common import get_logger
from common import JSON_INDENTATION
from common import PROVIDERS_FILE_PATH

XMPP_COMPLIANCE_TESTER_URL = "https://compliance.conversations.im/server/%s/"
XMPP_COMPLIANCE_TESTER_UPDATE_URL = "https://compliance.conversations.im/live/%s/"
GREEN_WEB_CHECK_URL = "https://admin.thegreenwebfoundation.org/greencheck/%s"

REQUEST_TIMEOUT_SECONDS = 10
MIN_UPDATE_TIMEOUT_SECONDS = 5
MAX_UPDATE_TIMEOUT_SECONDS = 10


class WebBotArgumentParser(ArgumentParser):
    """Parses arguments for WebBot."""

    def __init__(self) -> None:
        super().__init__()

        self.description = "Queries XMPP provider ratings or triggers their updates."
        self.usage = "%(prog)s [-h] | [-q | -d] [-u]"

        self.add_argument(
            "-q",
            "--quiet",
            help="log only errors",
            action="store_const",
            dest="log_level",
            const=logging.ERROR,
            default=logging.INFO,
        )

        self.add_argument(
            "-d",
            "--debug",
            help="log debug output",
            action="store_const",
            dest="log_level",
            const=logging.DEBUG,
            default=logging.INFO,
        )

        self.add_argument(
            "-u",
            "--update",
            action="store_true",
            help="trigger update of provider ratings (no query is run)",
        )


class WebBot:
    """Queries or updates ratings for providers in the providers file and writes
    changes to the providers file.
    """

    def __init__(self, update: bool) -> None:
        """For some services, an update trigger is available (using --update):
          - XMPP Compliance Tester
        Query/Update can only be run separately.

        Parameters
        ----------
        update : bool
            whether to trigger a provider update
        """

        with open(PROVIDERS_FILE_PATH, "rb") as providers_file:
            self._providers = json.load(providers_file)

        if update:
            self._trigger_updates()
            return

        self._check_providers()

    def _check_providers(self) -> None:
        """Checks the provider ratings for changes and writes them to the providers file
        .
        """

        log.info("Starting provider rating check")

        for name, data in self._providers.items():
            current_xmpp_compliance_tester = data["ratingXmppComplianceTester"][
                "content"
            ]
            # current_observatory_c2s = data[
            #    'ratingImObservatoryClientToServer']['content']
            # current_observatory_s2s = data[
            #    'ratingImObservatoryServerToServer']['content']

            # TODO: Use actual data as soon as implemented
            current_green_web_check = False

            new_xmpp_compliance_tester = self._get_xmpp_compliance_tester_rating(name)
            new_green_web_check = self._get_green_web_check(name)

            if (
                new_xmpp_compliance_tester is not None
                and new_xmpp_compliance_tester != current_xmpp_compliance_tester
            ):
                log.info(
                    "%s: XMPP Compliance Tester rating changed from %s to %s",
                    name,
                    current_xmpp_compliance_tester,
                    new_xmpp_compliance_tester,
                )
                data["ratingXmppComplianceTester"][
                    "content"
                ] = new_xmpp_compliance_tester

            if (
                new_green_web_check is not None
                and new_green_web_check != current_green_web_check
            ):
                log.info(
                    "%s: Green Web Check rating changed from %s to %s",
                    name,
                    current_green_web_check,
                    new_green_web_check,
                )
                # TODO: Write data

        with open(PROVIDERS_FILE_PATH, "w", encoding="utf-8") as providers_file:
            formatted_json = json.dumps(self._providers, indent=JSON_INDENTATION) + "\n"
            providers_file.write(formatted_json)

    @staticmethod
    def _get_xmpp_compliance_tester_rating(provider: str) -> int | None:
        """Queries XMPP Compliance Tester rating.

        Parameters
        ----------
        provider : str
            provider name

        Returns
        -------
        int | None
            provider rating or None on error
        """

        log.info("Querying XMPP Compliance Tester rating for %s", provider)

        url = XMPP_COMPLIANCE_TESTER_URL % provider
        log.debug("Sending request to %s", url)

        with requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS) as response:
            status_code = response.status_code

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Querying XMPP Compliance Tester rating for %s failed with status "
                    "code %s",
                    provider,
                    status_code,
                )
                return None

            soup = BeautifulSoup(response.text, "html.parser")
            result = soup.find("div", class_="stat_result")

            if result is None:
                return None

            return int(result.text.strip()[:-1])

    @staticmethod
    def _get_green_web_check(provider: str) -> bool | None:
        """Queries Green Web Check rating.

        Parameters
        ----------
        provider : str
            provider name

        Returns
        -------
        bool | None
            whether provider is rated as being green or None on error
        """

        log.info("Querying Green Web Check rating for %s", provider)

        url = GREEN_WEB_CHECK_URL % provider
        log.debug("Sending request to %s", url)

        with requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS) as response:
            status_code = response.status_code

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Querying Green Web Check rating for %s failed with status code "
                    "%s",
                    provider,
                    status_code,
                )
                return None

            return response.json().get("green")

    def _trigger_updates(self) -> None:
        """Triggers updates for provider ratings at services supporting updates.

        Currently supported:
        - XMPP Compliance Tester
        """

        log.info("Triggering provider rating updates")

        provider_count = len(self._providers)
        i = 1

        for provider in self._providers:
            self._update_xmpp_compliance_tester_rating(provider)

            if i < provider_count:
                i += 1
                timeout_seconds = random.randint(
                    MIN_UPDATE_TIMEOUT_SECONDS, MAX_UPDATE_TIMEOUT_SECONDS
                )

                log.debug(
                    "Waiting for %s seconds to trigger the next rating update",
                    timeout_seconds,
                )

                # Add timeout to avoid overloading the rating service.
                # The timeout is randomly chosen to avoid multiple bots querying the
                # same service with similar intervals.
                time.sleep(timeout_seconds)

    @staticmethod
    def _update_xmpp_compliance_tester_rating(provider: str) -> None:
        """Triggers an XMPP Compliance Tester check for updating a provider rating.

        Parameters
        ----------
        provider : str
            provider domain
        """

        log.info("Updating provider rating for %s", provider)

        url = XMPP_COMPLIANCE_TESTER_UPDATE_URL % provider
        log.debug("Sending request to %s", url)

        with requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS) as response:
            status_code = response.status_code

            if status_code != HTTPStatus.OK:
                log.warning(
                    "Updating provider rating for %s failed with status code: %s",
                    provider,
                    status_code,
                )


if __name__ == "__main__":
    arguments = WebBotArgumentParser().parse_args()
    log = get_logger(arguments.log_level)

    WebBot(arguments.update)
